#include "WProgram.h"

#include <avr/interrupt.h>
#include <avr/io.h>

unsigned int v; // variable to store the converted value
byte analogPin = 3; // analog input pin

// whether the current analog state is high or low
int state = 1;
// the number of high to low transitions
int count = 0;

//TODO set lower threshold to lower value for hysteresis
#define LOWERTHRESHOLD 700
#define UPPERTHRESHOLD 700

int samples = 0;
const int maxsamples = 8000;
bool busy = false;

// ===========================
//ISR(TIMER2_COMPA_vect)
ISR(TIMER2_COMPA_vect)
  {
  samples++;
  if (samples > maxsamples)
    {
    // turn off interrupts
    cli();
    // signal main loop we're done
    busy = false;
    }
  else
    {
    v = analogRead(analogPin);
    if (state == 1)
      {
      if (v <= LOWERTHRESHOLD)
        {
        count++;
        state = 0;
        }
      }
    else // state == 0
      {
      if (v > UPPERTHRESHOLD)
        {
        state = 1;
        }
      }
    }
  }

void setup_every125uS()
  {
  // Generating a square wave with a period twice the timeout
  //    125uS * 2 = 250uS period
  // should see this frequency on oscope:
  //    1 / 250 uS = 4000Hz

  // calculate number of ticks
  // the master clock is 16Mhz
  //    1 / 16Mhz = 62.5ns per tick
  // the timeout requires this many ticks
  //    125uS / 62.5nS = 2000 ticks
  // use prescalar of 8
  //    2000 / 8 = 250 prescalar ticks

  // set prescalar to "/8" = 010
  TCCR2B = _BV(CS21);

  // set WGM to CTC mode (010)
  // In this mode Timer2 counts up until it matches OCR2A
  TCCR2A = _BV(WGM21);

  // These are actual measurements from oscope:
  //  249 :  xx
  //  250 :  xx
  OCR2A = 249;

  // When the OCR2A register matches the Timer2 count, cause an interrupt
  TIMSK2 = _BV(OCIE2A);
  }

//===========================
void setup()
  {
  Serial.begin(9600); // open the serial port at 9600 bps:

  // make sure interrupts are off
  cli();

  // set up timer to interrupt every 125uS
  setup_every125uS();
  }

//===========================
void loop()
  {
  // interrupts are off at this point

  // kick off the sampling session
  busy = true;
  samples = 0;
  count = 0;
  //enable interrupts
  sei();
  // wait for sampling to finish
  while (busy)
    {
    delay(100);
    }

  // interrupts are off now, so it is safe to print
  Serial.print("v001 count = ");
  Serial.print(count);
  Serial.print("  rps= ");
  Serial.print((double)count / 4.0);
  Serial.print("  rpm= ");
  Serial.println((count * 60) / 4);
  }

//===========================
int main()
  {
  init();
  setup();
  for (;;)
    {
    loop();
    }
  return 0;
  }
